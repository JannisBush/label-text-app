import axios from 'axios/index';
import $ from 'jquery';
import config from './../../app/config'


let data;
let count = 0;
let current_template = "label";

const label_template = `
    <div class="pure-g">
        <div class="pure-u-1 center">
            <h1 id="title">${config.TITLE}</h1>
        </div>
        <div class="pure-u-4-5"></div>
        <div class="pure-u-1-5 pure-button pure-button-inactive">
            <button id="toggle" class="pure-button">See stats</button>
        </div>
        <div class="pure-u-4-5"></div>
        <div class="pure-u-1-5 center pure-button pure-button-inactive">
            Session count: <span id="count">${count}</span> items
        </div>
        <div class="pure-u-1">
            ${config.TEXTS.map(obj => `
                <div class="pure-u-1 space">
                <div class="pure-u-1-5 center">${obj.label}</div>
                <div class="pure-u-3-5" id=${obj.id}></div>
                </div>
            `).join('')}
        </div>
        
        <div class="pure-u-1 center">
            ${config.BUTTONS.map(button => 
                `<button id=${button} class="pure-button pure-button-primary space">${button}</button>`).join('')}
        </div>
     </div>`;

const stats_template = `
    <div class="pure-g">
     <div class="pure-u-1 center">
            <h1 id="title">Stats:</h1>
     </div>
     <div class="pure-u-4-5"></div>
     <div class="pure-u-1-5 pure-button pure-button-inactive">
         <button id="toggle" class="pure-button">Let's label</button>
     </div>
     
     <div class="pure-u-1">
         <div class="pure-u-1-3"></div>
         <div class="pure-u-1-3">
             <table class="pure-table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Labelled</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    ${config.USERS.map(user => 
                        `<tr>
                            <td>${user}</td>
                            <td id="count${user}"></td>
                            <td id="total${user}"></td>
                        </tr>`
                        ).join('')}
                </tbody>  
             </table>
         </div>
     </div>
     
    </div>`;


const next_tweet = function() {
    axios.get(`${window.location.href}api/get_next`).then((response) =>{
        data = response.data;
        if (data !== null){
            for (const elem of config.TEXTS) {
                let text = data;
                for (const x of elem.source) {
                    text = text[x];
                }
                $(`#${elem.id}`).text(text);
            };

        }
        else {
            $("#title").text("Currently there is nothing more to label for you!");
            for (const elem of config.TEXTS) {
                $(`#${elem.id}`).text("");
            };
            for (const button of config.BUTTONS) {
                $(`#${button}`).addClass("invisible");
            };

        }

    });
};



const update_data = function(e) {
    if (data === null) {
        return
    }
    let uri = `${window.location.href}api/update/${data._id}/?label=${e.target.id}`;
    axios.get(uri).then((response) => {
        count = count + 1;
        $("#count").text(count);
        next_tweet();
    });

};

const get_stats = function() {
    axios.get(`${window.location.href}api/get_total_stats`).then((response) => {
        let stats = response.data;
        if (stats !== null) {
            for (let i in stats[0]){
                const user = config.USERS[i];
                const total = stats[0][i];
                const done = total - stats[1][i] //we get remaining, but want done
                $(`#count${user}`).text(done);
                $(`#total${user}`).text(total);
            }
        }
    });
};

const toggle = function() {
    if (current_template === "label"){
        $("#main").html(stats_template);
        $("#toggle").on("click", toggle);
        get_stats();
        current_template = "stats";
    } else {
        $("main").html(label_template);
        $("#toggle").on("click", toggle);
        next_tweet();
        for (const button of config.BUTTONS){
            $(`#${button}`).on("click", update_data);
        };
        current_template = "label";
    }
};
document.addEventListener("DOMContentLoaded", function(event) {
    $("#main").html(label_template);

    next_tweet();

    for (const button of config.BUTTONS){
        $(`#${button}`).on("click", update_data);
    };
    
    
    $("#toggle").on("click", toggle);


});