// server.js

// all imports
const express = require('express');
const favicon = require('express-favicon');
const path = require('path');
const basicAuth = require('express-basic-auth');
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const config = require('./app/config');


const port = process.env.PORT || 8080;
const app = express();

// Use the favicon defined in ./public
app.use(favicon('./public/favicon.ico'));

// Require the user to be logged in, there are three users and the passwords are defined in env variables
app.use(basicAuth({
    users: get_users(),
    /* {
     'admin': process.env.ADMIN,
     'user1': process.env.USER1,
     'user2': process.env.USER2
 },*/
    challenge: true,
    unauthorizedResponse: get_unauthorized_response,
    realm: 'LabelRealm'
}));

// Send static files from current_dir/public
app.use(express.static(path.join(__dirname, '/public')));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({extended: true}));


// Reply for unauthorized users
function get_unauthorized_response(req) {
    return req.auth
        ? (`User ${req.auth.user}  rejected`)
        : 'No credentials provided'
}

function get_users() {
    const users = {};
    for (x of config.USERS){
        users[x] = process.env[x];
    };
    return users;
}

// Connect to database (configured in env variable)
mongoose.connect(process.env.DB_URI, {useNewUrlParser: true});

const data_routes = require('./app/routes');

//  Use routes defined in data_routes.js and prefix it with api
app.use('/api', data_routes);


// Serve index.html when request to the root is made
app.route('/').get( (req, res) => {
    console.log(req);
    console.log(res);
    res.sendFile('./public/index.html');
});


app.listen(port);
console.log('App listening on port ' + port);


// Use morgan to log request in dev mode
app.use(morgan('dev'));