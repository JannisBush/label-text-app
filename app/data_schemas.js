const mongoose = require('mongoose');

// Define collections and schemas for data

const data_schema = new mongoose.Schema({
    label: {
        type: String
    }
});

let last_collection_name;
let current_model;

const return_correct_model = function(collection_name){
    if (last_collection_name !== collection_name){
        last_collection_name = collection_name;
        console.log("Create new Model");
        current_model = mongoose.model(collection_name, data_schema, collection_name);

    }
    return current_model
}


module.exports = return_correct_model;