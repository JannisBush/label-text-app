
// Config for relation labelling, you have an original post and a reply and you want to label the relation between them
module.exports = {
    TITLE: "Have fun labelling!",
    USERS: ["user1", "user2", "user3"],
    BUTTONS: ["Attack", "Support", "Other"],
    TEXTS: [{id: "org", label: "Original Post:", source: ["org", "full_text"]},
        {id: "reply", label: "Response:", source: ["reply", "full_text"]}]
} 


// Config for sentiment labelling, you have one post and want to label the sentiment
/*
module.exports = {
    TITLE: "Give the sentiment",
    USERS: ["admin", "user1", "user2"],
    BUTTONS: ["-1", "0", "1"],
    TEXTS: [{id: "org", label: "Original Post:", source: ["full_text"]}] 
}*/
 