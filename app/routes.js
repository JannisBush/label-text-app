const express = require('express');
const data_routes = express.Router();
const data_schemas = require('./data_schemas');
const config = require('./config');


data_routes.route('/get_stats').get(async function (req, res, next) {
    let total;
    let remaining;
    await data_schemas(req.auth.user).countDocuments(function (err, count) {
        if (err) {
            return next(new Error(err))
        }
        total = count;
    });
    data_schemas(req.auth.user).countDocuments({label: "undefined"}, function (err, count) {
        if (err) {
            return next(new Error(err))
        }
        remaining = count;
        res.json({"done": total-remaining, "total": total});
    });
});

data_routes.route('/get_total_stats').get(async function (req, res, next) {
    // allow everyone to see the stats for now
    /*if (req.auth.user !== 'admin') {
        return res.status(401).send("Only admin is allowed to see the total stats")
    }*/
    let total = [];
    let remaining = [];
    let last_user = config.USERS.slice(-1)[0]
    for (const user of config.USERS) {
        await data_schemas(user).countDocuments(function (err, count) {
            if (err) {
                return next(new Error(err))
            }
            total.push(count);
        });
        data_schemas(user).countDocuments({label: "undefined"}, function (err, count) {
            if (err) {
                return next(new Error(err))
            }
            remaining.push(count);
            if (user === last_user){
                res.json([total, remaining]);
            }

        });
    }
});

data_routes.route('/get_next').get(function (req, res, next) {
    data_schemas(req.auth.user).findOne({label: "undefined"}, function(err, entry) {
        if (err) {
            return next(new Error(err))
        }
        res.json(entry);

    })
});

data_routes.route('/update/:id').get(function (req, res, next) {
    const id = req.params.id;
    data_schemas(req.auth.user).findById(id, function (error, doc) {
        if (error) {
            return next(new Error('Document not found'))
        } else {
            console.log(req.query.label);
            doc.label = req.query.label;
            doc.save(
                function (error, doc) {
                if (error) {
                    res.status(400).send('Unable to update document');
                } else {
                    res.status(200).json('Updated document');
                }
            });
        }
    });
});

module.exports = data_routes;