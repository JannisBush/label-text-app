// webpack.config.js
const resolve = require('path').resolve;

module.exports = {
    entry: [
        './public/src/index.js',
        './public/src/index.css',
        './public/src/pure-min.css'
    ],
    output: {
        path: resolve('public/build'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader",
                    }
                ]
            },
        ]
    }
};